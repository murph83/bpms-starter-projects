# BPM Suite Starter Projects
These projects contain a `pom.xml` that is a **bare minimum** client project for connecting to business-central for each of the releases of Red Hat JBoss BPM Suite. Additional dependencies may be required for specific remote functionality.

They rely on the use of a BOM, as described in [this KB article](https://access.redhat.com/solutions/2129131).

There is also a `no-bom.pom.xml` file which represents the same minimal pom, but without the use of the BOM to manage project versions. Please note, this means that third-party dependencies such as Hibernate will have to have their versions managed explicitly to ensure compatibility with the KIE, Drools and jBPM libraries. Please reference the BOMs for insight into compatible versions.

Versions <= 6.0.3 do not have BOMs, and so only have `no-bom.pom.xml` files


Red Hat BPM Suite Version | Maven BOM | jBPM Engine Version
---|---|---
6.0.1 | N/A	| 6.0.2-redhat-6
6.0.2 | N/A	| 6.0.3-redhat-4
6.0.3 | N/A	| 6.0.3-redhat-6
6.1.0 | [6.1.0.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.1.0.GA-redhat-2/) | 6.2.0.Final-redhat-4
6.1.1 | [6.1.1.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.1.1.GA-redhat-2/) | 6.2.0.Final-redhat-6
6.1.2 | [6.1.2.GA-redhat-3/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.1.2.GA-redhat-3/) | 6.2.0.Final-redhat-9
6.1.3 | [6.1.3.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.1.3.GA-redhat-2/) | 6.2.0.Final-redhat-11
6.1.4 | [6.1.4.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.1.4.GA-redhat-2/) | 6.2.0.Final-redhat-13
6.1.5 | [6.1.5.GA-redhat-1/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.1.5.GA-redhat-1/) | 6.2.0.Final-redhat-14
6.2.0 | [6.2.0.GA-redhat-1/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.1.0.GA-redhat-1/) | 6.3.0.Final-redhat-5
6.2.1 | [6.2.1.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.2.1.GA-redhat-2/) | 6.3.0.Final-redhat-7
6.2.2 | [6.2.2.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.2.2.GA-redhat-2/) | 6.3.0.Final-redhat-9
6.2.3 | [6.2.3.GA-redhat-3/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.2.3.GA-redhat-3/) | 6.3.0.Final-redhat-12
6.3.0 | [6.3.0.GA-redhat-3/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.3.0.GA-redhat-3/) | 6.4.0.Final-redhat-3
6.3.1 | [6.3.1.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.3.1.GA-redhat-2/) | 6.4.0.Final-redhat-6
6.3.2 | [6.3.2.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.3.2.GA-redhat-2/) | 6.4.0.Final-redhat-8
6.3.3 | [6.3.3.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.3.3.GA-redhat-2/) | 6.4.0.Final-redhat-10
6.4.0 | [6.4.0.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.4.0.GA-redhat-2/) | 6.5.0.Final-redhat-2
6.4.1 | [6.4.1.GA-redhat-3/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.4.1.GA-redhat-3/) | 6.5.0.Final-redhat-5
6.4.2 | [6.4.2.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.4.2.GA-redhat-2/) | 6.5.0.Final-redhat-7
6.4.3 | [6.4.3.GA-redhat-2/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.4.3.GA-redhat-2/) | 6.5.0.Final-redhat-9
6.4.4 | [6.4.4.GA-redhat-3/](https://maven.repository.redhat.com/ga/org/jboss/bom/brms/jboss-brms-bpmsuite-bom/6.4.4.GA-redhat-3/) | 6.5.0.Final-redhat-12
